import { Route } from '@angular/router';

import { AuthGuard } from '@shared/guards';
import { RulesEnum } from '@shared/models';

export const routes: Route[] = [
	/**Header */
	/**главная Лента */
	{
		path: '',
		loadComponent: () => import('./pages/home/home.component').then((e) => e.HomeComponent),
	},
	{
		path: 'auth',
		loadComponent: () => import('./pages/auth/auth.component').then((c) => c.AuthComponent),
	},
	{
		path: 'admin',
		loadComponent: () => import('./pages/admin/admin.component').then((c) => c.AdminComponent),
		loadChildren: () => import('./pages/admin/admin.route').then((r) => r.ADMIN_ROUTES),
		canMatch: [AuthGuard(RulesEnum.SUPER_ADMIN, RulesEnum.ADMIN)],
	},
	/**Трансляции */
	{
		path: 'broadcast',
		loadComponent: () => import('./pages/broadcast/broadcast.component').then((e) => e.BroadcastComponent),
	},
	/**Лучшие букмекеры */
	{
		path: 'bookmakers',
		loadComponent: () => import('./pages/bookmakers/bookmakers.component').then((e) => e.BookmakersComponent),
	},
	/**Обзоры*/
	{
		path: 'overview',
		loadComponent: () => import('./pages/overview/overview.component').then((e) => e.OverviewComponent),
	},
	/**footer */
	/**Контакты */
	{
		path: 'contacts',
		loadComponent: () => import('./pages/contacts/contacts.component').then((e) => e.ContactsComponent),
	},
	/**Про нас */
	{
		path: 'about',
		loadComponent: () => import('./pages/about/about.component').then((e) => e.AboutComponent),
	},
	/**Реклама */
	{
		path: 'advertisement',
		loadComponent: () =>
			import('./pages/advertisement/advertisement.component').then((e) => e.AdvertisementComponent),
	},
	/**Пользовательское соглашение */
	{
		path: 'agreement',
		loadComponent: () => import('./pages/agreement/agreement.component').then((e) => e.AgreementComponent),
	},
	/**Политика конфиденциальности */
	{
		path: 'politics',
		loadComponent: () => import('./pages/politics/politics.component').then((e) => e.PoliticsComponent),
	},
	/**Для правообладателей */
	{
		path: 'copyright',
		loadComponent: () => import('./pages/copyright/copyright.component').then((e) => e.CopyrightComponent),
	},

	{ path: '**', redirectTo: '' },
];
