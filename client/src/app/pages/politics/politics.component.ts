import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-politics',
    standalone: true,
    imports: [
        CommonModule,
    ],
    templateUrl: './politics.component.html',
    styleUrl: './politics.component.less',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PoliticsComponent { }
