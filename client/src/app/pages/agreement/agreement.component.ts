import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-agreement',
    standalone: true,
    imports: [
        CommonModule,
    ],
    templateUrl: './agreement.component.html',
    styleUrl: './agreement.component.less',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AgreementComponent { }
