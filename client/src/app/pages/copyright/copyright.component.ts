import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-copyright',
    standalone: true,
    imports: [
        CommonModule,
    ],
    templateUrl: './copyright.component.html',
    styleUrl: './copyright.component.less',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CopyrightComponent { }
