import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { AuthService } from '@shared/services';

@Component({
    selector: 'app-auth',
    standalone: true,
    imports: [
        CommonModule,
    ],
    template: `<p>
        Авторизация
    </p>`,
    styleUrl: './auth.component.less',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthComponent {
    private readonly authService = inject(AuthService)
    onClick(){
        this.authService.signIn({login:'test',password:'test'})
    }
 }
