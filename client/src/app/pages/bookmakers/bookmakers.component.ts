import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BlockMatchesComponent } from 'src/app/shared/components/block-matches/block-matches.component';
import { ButtonMarketingComponent } from 'src/app/shared/components/button-marketing/button-marketing.component';
import { ColumnsComponent } from 'src/app/shared/components/columns/columns.component';

@Component({
	selector: 'app-bookmakers',
	standalone: true,
	imports: [BlockMatchesComponent, ColumnsComponent, ButtonMarketingComponent],
	templateUrl: './bookmakers.component.html',
	styleUrl: './bookmakers.component.less',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookmakersComponent {}
