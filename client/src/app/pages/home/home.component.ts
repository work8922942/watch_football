import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BlockMatchesComponent } from 'src/app/shared/components/block-matches/block-matches.component';
import { ButtonMarketingComponent } from 'src/app/shared/components/button-marketing/button-marketing.component';
import { ColumnsComponent } from 'src/app/shared/components/columns/columns.component';
import { InfoComponent, InfoType } from 'src/app/shared/components/info/info.component';

@Component({
	selector: 'app-home',
	standalone: true,
	imports: [BlockMatchesComponent,ColumnsComponent,ButtonMarketingComponent,InfoComponent],
	templateUrl: './home.component.html',
	styleUrl: './home.component.less',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent {
  infoData:InfoType={img:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT5eTto3Qx83dgQalSmi_nROsP-QZAc2t2gNA&s',shot:'Бразильцы разобрались с испанцами в товарищеском матче.',title:'Испания проиграла сборной Бразилии',type:'Новости'}
}
