import { Route } from '@angular/router';

export const ADMIN_ROUTES: Route[] = [
  // {
  //   path: 'dashboards',
  //   loadComponent: () => import('./pages/dashboards/dashboards.component').then((mod) => mod.default),
  //   data: { breadcrumb: 'Дашборды', animation: 'dashboards' },
  // },
  // {
  //   path: 'resurs',
  //   loadChildren: () => import('./pages/resurs/resurs.route').then((r) => r.RESURS_ROUTES),
  //   data: { animation: 'resurs' },
  // },
  // {
  //   path: 'settings',
  //   loadChildren: () => import('./pages/settings/settings.route').then((r) => r.SETTINGS_ROUTES),
  //   data: { animation: 'settings' },
  // },
  {
    path: '**',
    redirectTo: '',
  },
];
