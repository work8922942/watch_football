import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-advertisement',
    standalone: true,
    imports: [
        CommonModule,
    ],
    templateUrl: './advertisement.component.html',
    styleUrl: './advertisement.component.less',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdvertisementComponent { }
