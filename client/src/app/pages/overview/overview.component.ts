import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-overview',
    standalone: true,
    imports: [
        CommonModule,
    ],
    template: `<p>overview works!</p>`,
    styleUrl: './overview.component.less',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OverviewComponent { }
