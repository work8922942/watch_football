import { trigger, transition, style, animate, stagger, query, group } from '@angular/animations';

const TIMING = '0.2s';

export const SlideLeftToRightAnimation = (timing: string = TIMING) =>
  trigger('slideLeftToRight', [
    transition(':enter', [
      style({ opacity: 0, width: 0, 'max-width': 0, 'min-width': 0, overflow: 'hidden' }),
      animate(`${timing} ease-out`, style({ opacity: 1, width: '*', 'max-width': '*', 'min-width': '*' })),
    ]),
    transition(':leave', [
      style({ opacity: 1, width: '*', 'max-width': '*', 'min-width': '*', overflow: 'hidden' }),
      animate(`${timing} ease-out`, style({ opacity: 0, width: 0, 'max-width': 0, 'min-width': 0 })),
    ]),
  ]);

export const SlideTopToBottomAnimation = (timing: string = TIMING) =>
  trigger('slideTopToBottom', [
    transition(':enter', [
      style({ opacity: 0, transform: 'translateY(-20%)' }),
      animate(`${timing} ease-out`, style({ opacity: 1, transform: 'translateY(0)' })),
    ]),
  ]);

export const SlideBottomToTopAnimation = (timing: string = TIMING) =>
  trigger('slideBottomToTop', [
    transition(':enter', [
      style({ opacity: 0, transform: 'translateY(20%)' }),
      animate(`${timing} ease-out`, style({ opacity: 1, transform: 'translateY(0)' })),
    ]),
  ]);
export const SlideTopToBottomStaggerAnimation = (timing: number = 500) =>
  trigger('slideTopToBottomStagger', [
    transition(':enter', [
      group([
        query(
          '.animate',
          [
            style({ opacity: 0, transform: 'translateY(-20%)' }),
            stagger(timing, [
              animate(`${timing}ms cubic-bezier(0.35, 0, 0.25, 1)`, style({ opacity: 1, transform: 'translateY(0)' })),
            ]),
          ],
          { optional: true },
        ),
        query(
          '.animateRevert',
          [
            style({ opacity: 0, transform: 'translateY(120%)' }),
            stagger(timing, [
              animate(`${timing}ms cubic-bezier(0.35, 0, 0.25, 1)`, style({ opacity: 1, transform: 'translateY(0)' })),
            ]),
          ],
          { optional: true },
        ),
      ]),
    ]),
  ]);

export const SlideLeftRightAnimation = trigger('slideLeftRight', [
  transition(':enter', [
    style({ opacity: 0, position: 'absolute', transform: 'translateX(50%)' }),
    animate(`0.5s ease-out`, style({ opacity: 1, transform: 'translateX(0)' })),
  ]),
  transition(':leave', [
    style({ opacity: 1, position: 'absolute', transform: 'translateX(0)' }),
    animate(`0.5s ease-out`, style({ opacity: 0, transform: 'translateX(-50%)' })),
  ]),
]);
