export type LocalFileShared = {
  filename: string;
  path: string;
  mimetype: string;
  id: number;
};
