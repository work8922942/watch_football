import { User } from './user.shared';

export enum AuthProviders {
  GOOGLE = 'google',
  YANDEX = 'yandex',
  VK = 'vk',
}

export type AuthRequest = {
  login: string;
  password: string;
};

export type AuthResponse = {
  user: User;
  access_token: string;
};
