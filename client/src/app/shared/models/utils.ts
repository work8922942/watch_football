export class Utils {
	// https://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid
	static uuidv4() {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
			var r = (Math.random() * 16) | 0,
				v = c == 'x' ? r : (r & 0x3) | 0x8;
			return v.toString(16);
		});
	}
	static getTextError(error: any, defaultText = 'Произошла ошибка') {
		if (typeof error === 'string') {
			return error;
		}
		if (typeof error === 'object') {
			if (typeof error?.error === 'string') {
				return error?.error;
			}
			if (typeof error?.message === 'string') {
				return error?.message;
			}
		}
		return defaultText;
	}
	static async getBase64(file: File) {
		const reader = new FileReader();
		reader.readAsDataURL(file);
		return new Promise<string>((res, rej) => {
			reader.onload = function () {
				res(reader.result as string);
			};
			reader.onerror = function (error) {
				rej(`Error: ${error}`);
			};
		});
	}

	static getCookie(name: string) {
		let matches = document.cookie.match(
			new RegExp('(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'),
		);
		return matches ? decodeURIComponent(matches[1]) : undefined;
	}
	static deleteCookie(name: string) {
		document.cookie = `${name}=;expires='${new Date(0).toUTCString()};path=/`;
	}

	static getRandomColor() {
		// https://css-tricks.com/snippets/javascript/random-hex-color/
		return `#${Math.floor(Math.random() * 16777215).toString(16)}`;
	}
}
