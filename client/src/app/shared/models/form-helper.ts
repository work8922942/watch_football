import { FormArray, FormControl, FormGroup } from '@angular/forms';

type ArrayType<T> = T extends unknown[] ? T[0] : T;

export type FormHelper<T> = {
  [K in keyof T]:
    | FormControl<T[K] | null | undefined>
    | FormArray<FormGroup<FormHelper<ArrayType<T[K]>>>>
    | FormArray<FormControl<ArrayType<T[K]>>>;
};

export const SelectSeparators = [',', ', ', ' ,', ' , ', ';', ' ;', '; '];
