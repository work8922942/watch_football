import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-button-marketing',
    standalone: true,
    imports: [
        CommonModule,
    ],
    templateUrl: './button-marketing.component.html',
    styleUrl: './button-marketing.component.less',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonMarketingComponent { }
