import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ColumnsComponent } from '../columns/columns.component';
import { RouterLink } from '@angular/router';

@Component({
    selector: 'app-footer',
    standalone: true,
    imports: [
        CommonModule,
        ColumnsComponent,
        RouterLink
    ],
    templateUrl: './footer.component.html',
    styleUrl: './footer.component.less',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent { }
