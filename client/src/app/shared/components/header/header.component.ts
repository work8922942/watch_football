import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, signal } from '@angular/core';
import { LoginButtonComponent } from '../login-button/login-button.component';
import { RouterLink } from '@angular/router';

@Component({
	selector: 'app-header',
	standalone: true,
	imports: [CommonModule, LoginButtonComponent,RouterLink],
	template: `
		<div class="header">
			<div class="logo" [routerLink]="['/']">
				<img width="121" src="assets/images/header/logo.png" alt="logo" />
			</div>
			<div class="menu" [ngClass]="{ active: isActive() }">
				<div class="back" (click)="isActive.set(!isActive())"></div>
				<nav class="nav">
					<ul>
						<li [routerLink]="['/']">Лента</li>
						<li [routerLink]="['/broadcast']" (click)="routerLink('/broadcast')">Трансляции</li>
						<li [routerLink]="['/bookmakers']" (click)="routerLink('/bookmakers')">Лучшие букмекеры</li>
						<li [routerLink]="['/overview']" (click)="routerLink('/overview')">Обзоры</li>
					</ul>
				</nav>
				<div class="login">
					<app-login-button></app-login-button>
				</div>
			</div>

			<div class="menu-burger" (click)="isActive.set(!isActive())">
				<div class="lines">
					<span class="line" [ngClass]="{ cross: isActive() }"></span>
				</div>
			</div>
		</div>
	`,
	styleUrl: './header.component.less',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
	protected readonly isActive = signal<boolean>(false);
	routerLink(link:string){

	}
}
