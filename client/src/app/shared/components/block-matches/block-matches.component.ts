import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, LOCALE_ID } from '@angular/core';

@Component({
    selector: 'app-block-matches',
    standalone: true,
    imports: [
        CommonModule,
    ],

    templateUrl: './block-matches.component.html',
    styleUrl: './block-matches.component.less',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BlockMatchesComponent {

  data=[
    {
      time: new Date("2024-10-10T14:48:00"),
      first:'Испания',
      second:'Бразилия'
    },
    {
      time: new Date("2024-09-10T14:48:00"),
      first:'Тоттенхэм',
      second:'Ньюкасл'
    },
    {
      time: new Date("2024-08-10T14:48:00"),
      first:'Ман Сити',
      second:'Ман Юнайтед'
    },
    {
      time: new Date("2024-07-10T14:48:00"),
      first:'Челси',
      second:'Ливерпуль'
    },
    {
      time: new Date("2024-06-10T14:48:00"),
      first:'Испания',
      second:'Бразилия'
    },
    {
      time: new Date("2024-05-10T14:48:00"),
      first:'Тоттенхэм',
      second:'Ньюкасл'
    },
    {
      time: new Date("2024-04-10T14:48:00"),
      first:'Ман Сити',
      second:'Ман Юнайтед'
    },
    {
      time: new Date("2024-04-10T14:48:00"),
      first:'Челси',
      second:'Ливерпуль'
    },
    {
      time: new Date("2024-04-10T14:48:00"),
      first:'Испания',
      second:'Бразилия'
    },
    {
      time: new Date("2024-04-10T14:48:00"),
      first:'Тоттенхэм',
      second:'Ньюкасл'
    },
    {
      time: new Date("2024-04-10T14:48:00"),
      first:'Ман Сити',
      second:'Ман Юнайтед'
    },
    {
      time: new Date("2024-04-10T14:48:00"),
      first:'Челси',
      second:'Ливерпуль'
    },
  ]

}
