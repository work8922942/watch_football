import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input, input } from '@angular/core';

export type InfoType={title:string,shot:string,img:string,type:string}

@Component({
    selector: 'app-info',
    standalone: true,
    imports: [
        CommonModule,
    ],
    templateUrl: './info.component.html',
    styleUrl: './info.component.less',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoComponent {
  @Input() infoData!:InfoType
}
