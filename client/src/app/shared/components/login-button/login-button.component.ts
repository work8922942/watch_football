import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject, signal } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { AuthService } from '@shared/services';

@Component({
    selector: 'app-login-button',
    standalone: true,
    imports: [
        CommonModule,
        RouterLink,
    ],
    template: `<button class="login-button" type="button" (click)="onClick(isAuth())" >{{!isAuth()?'Войти':'Выйти'}}</button>`,
    styleUrl: './login-button.component.less',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginButtonComponent {
  private readonly router = inject(Router);
    isAuth=signal<boolean>(false);
    private readonly authService = inject(AuthService)
    onClick(isAuth:boolean){
        this.authService.signIn({login:'test',password:'test'})
        // this.isAuth.set(this.authService.isAuth);
        this.isAuth.set(!isAuth);
        this.router.navigate(['/auth']);


    }
}
