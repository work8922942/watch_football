import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-columns',
    standalone: true,
    imports: [
        CommonModule,
    ],
    templateUrl: './columns.component.html',
    styleUrl:  './columns.component.less',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ColumnsComponent { }
