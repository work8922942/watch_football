import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { HeaderComponent } from './shared/components/header/header.component';
import { FooterComponent } from './shared/components/footer/footer.component';

@Component({
	selector: 'app-root',
	template: `
		<div class="wrapper">
			<div class="container">
				<app-header></app-header>
				<div class="layout">
					<router-outlet></router-outlet>
				</div>
				<div class="footer">
					<app-footer></app-footer>
				</div>
			</div>
		</div>
	`,
	styles: `
  .wrapper {
		width: 100%;
		min-height: 100%;
		background-color: #fff;
	}
	.container {
		width: 100%;
		max-width: 1200px;
		padding: 10px 10px 0 10px;
		background-color: #fff;
		margin-inline: auto;
    box-sizing: border-box;
    // margin: 0;

    height: 100vh;
    display: grid;
    grid-template-rows: auto 1fr auto;
	}

	.layout {
		// flex-grow: 1;
	}
	.footer {
		// flex-shrink: 0;
	}

  `,
	standalone: true,
	imports: [RouterOutlet, HeaderComponent, FooterComponent],
})
export class AppComponent {}
