import { inject } from '@angular/core';
import { AuthService } from '@shared/services';

export function AppInitialize(): () => Promise<boolean> {
  const auth = inject(AuthService);


  return async () => {



    const tokenFromLs = auth.accessToken();
    if (!tokenFromLs) {
      return true;
    }
    try {
      await auth.tokenSignIn({ access_token: tokenFromLs });
    } catch (error) {
      console.error(error);
      auth.logout(false);
    }
    return true;
  };
}
