<p align="center">
  <a href="https://angular.io/" target="blank"><img src="https://angular.io/assets/images/logos/angular/angular.svg" width="170" alt="Angular Logo" /></a>
</p>

## Установка зависимостей и запуск
Перейдём в папку с клиентом: 
```bash
cd client
```
Перед запуском проекта необходимо установить зависимости:
```bash
npm install
```



Если исправлять клиент, то необходимо установить расширение для `vscode` - `Prettier - Code formatter`. [marketplace](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode).
Данное расширение для того чтобы был одинаковый формат кода при сохранении.

Запуск клиента: 
```bash
npm run start
или 
ng serve --port 4200
```
